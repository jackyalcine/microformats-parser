use html5ever::parse_document;
use html5ever::driver::ParseOpts;
use html5ever::rcdom::RcDom;
use html5ever::tendril::TendrilSink;

use serde::{Serialize, Deserialize};
use std::io;

use super::parser;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Root {
    pub items: Vec<Microformat>
}

impl Root {
    pub fn new () -> Root {
        Root{
            items: Vec::new()
        }
    }

    /// Construct HTML from RcDom, optionally with a URL set
    pub fn from_dom(dom: RcDom) -> Self {
        let parser = parser::Parser::start(dom.document);
        let mut root = Root::new();
        let mut context = parser::ParserContext::Root {
            item: &mut root
        };
        parser.traverse(&mut context);

        match context {
            parser::ParserContext::Root {item} => item.clone(),
            _ => unreachable!()
        }
    }

    pub fn from_string(html: String) -> Result<Self, io::Error> {
        parse_document(RcDom::default(), ParseOpts::default())
            .from_utf8()
            .read_from(&mut html.as_bytes())
            .and_then(|dom| Ok(Self::from_dom(dom)))
    }
}



#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(untagged)]
pub enum Microformat {
    HEntry {
        r#type: Vec<String>,
        properties: Box<HEntryProps>
    },
    HCard {
        r#type: Vec<String>,
        value: String,
        properties: HCardProps
    },
    HCite {
        r#type: Vec<String>,
        properties: Box<HCiteProps>
    }
}

impl Microformat {
    pub fn new (t :&str) -> Microformat {
        match t {
            "h-cite" => Microformat::HCite {
                r#type: Vec::from([String::from("h-cite")]),
                properties: Box::new(HCiteProps::new())
            },
            "h-entry" => Microformat::HEntry {
                r#type: Vec::from([String::from("h-entry")]),
                properties: Box::new(HEntryProps::new())
            },
            "h-card" => Microformat::HCard {
                r#type: Vec::from([String::from("h-card")]),
                value: String::new(),
                properties: HCardProps::new()
            },
            _ => unreachable!()
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct HEntryProps {
    pub author: Microformat,
    pub category: Vec<String>,
    pub comment: Vec<Microformat>,
    pub content: EContent,
    pub published: Vec<String>,
    pub updated: Vec<String>,
    pub in_reply_to: Vec<Microformat>,
    pub like_of: Vec<Microformat>,
    pub name: Vec<String>,
    pub repost_of: Vec<Microformat>,
    pub summary: Vec<String>,
    pub syndication: Vec<String>,
    pub url: Vec<String>,
}

impl HEntryProps {
    pub fn new() -> HEntryProps {
        HEntryProps {
            author: Microformat::new("h-card"),
            category: Vec::new(),
            comment: Vec::new(),
            content: EContent::new(),
            in_reply_to: Vec::new(),
            like_of: Vec::new(),
            name: Vec::new(),
            published: Vec::new(),
            repost_of: Vec::new(),
            summary: Vec::new(),
            syndication: Vec::new(),
            updated: Vec::new(),
            url: Vec::new(),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct EContent {
    pub value: String,
    pub html: String
}

impl EContent {
    pub fn new () -> EContent {
        EContent {
            value: String::new(),
            html: String::new()
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct HCardProps {
    pub name: Vec<String>,
    pub url: Vec<String>,
}

impl HCardProps {
    pub fn new() -> HCardProps {
        HCardProps {
            name: Vec::new(),
            url: Vec::new(),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct HCiteProps {
    pub author: Microformat,
    pub content: Vec<String>,
    pub name: Vec<String>,
    pub published: Vec<String>,
    pub url: Vec<String>,
}

impl HCiteProps {
    pub fn new() -> HCiteProps {
        HCiteProps {
            author: Microformat::new("h-card"),
            content: Vec::new(),
            name: Vec::new(),
            published: Vec::new(),
            url: Vec::new(),
        }
    }
}

