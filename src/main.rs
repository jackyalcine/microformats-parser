use std::env;
use std::fs;

mod mapparser;
// mod parser;
// mod microformats;

fn main() {

    let args: Vec<String> = env::args().collect();

    let filename = &args[1];

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let root = mapparser::parse_string(contents);

    println!("{}", serde_json::to_string_pretty(&root).unwrap());
}
