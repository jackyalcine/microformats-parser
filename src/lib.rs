#[cfg(test)]
use assert_json_diff::assert_json_eq;
#[cfg(test)]
use serde_json::json;

// mod parser;
// pub mod microformats;
pub mod mapparser;


#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn person_example() {
        let content = String::from("<span class=\"h-card\">Frances Berriman</span>");
        let root = super::mapparser::parse_string(content);
        let expected = json!({
            "items": [{ 
                "type": ["h-card"],
                "properties": {
                    "name": ["Frances Berriman"] 
                }
            }]
        });
        println!("{}", serde_json::to_string_pretty(&root).unwrap());

        assert_json_eq!(root, expected);
    }

    #[test]
    fn hyperlinked_person() {
        let content = String::from("<a class=\"h-card\" href=\"http://benward.me\">Ben Ward</a>");
        let root = super::mapparser::parse_string(content);
        let expected = json!({
            "items": [{ 
                "type": ["h-card"],
                "properties": {
                    "name": ["Ben Ward"],
                    "url": ["http://benward.me"]
                }
            }]
        });
        println!("{}", serde_json::to_string_pretty(&root).unwrap());
        assert_json_eq!(root, expected);
    }

    #[test]
    fn hyperlinked_person_image() {
        let content_s = "<a class=\"h-card\" href=\"http://rohit.khare.org/\">
 <img alt=\"Rohit Khare\"
      src=\"https://s3.amazonaws.com/twitter_production/profile_images/53307499/180px-Rohit-sq_bigger.jpg\" />
</a>";
        let content = String::from(content_s);
        let root = super::mapparser::parse_string(content);
        let expected = json!({
            "items": [{ 
                "type": ["h-card"],
                "properties": {
                    "name": ["Rohit Khare"],
                    "url": ["http://rohit.khare.org/"],
                    "photo": ["https://s3.amazonaws.com/twitter_production/profile_images/53307499/180px-Rohit-sq_bigger.jpg"]
                }
            }]
        });
        println!("{}", serde_json::to_string_pretty(&root).unwrap());
        assert_json_eq!(root, expected);
    }

    #[test]
    fn detailed_person_example() {
        let content_s = "<div class=\"h-card\">
  <img class=\"u-photo\" alt=\"photo of Mitchell\"
       src=\"https://webfwd.org/content/about-experts/300.mitchellbaker/mentor_mbaker.jpg\"/>
  <a class=\"p-name u-url\"
     href=\"http://blog.lizardwrangler.com/\" 
    >Mitchell Baker</a>
 (<a class=\"u-url\" 
     href=\"https://twitter.com/MitchellBaker\"
    >@MitchellBaker</a>)
  <span class=\"p-org\">Mozilla Foundation</span>
  <p class=\"p-note\">
    Mitchell is responsible for setting the direction and scope of the Mozilla Foundation and its activities.
  </p>
  <span class=\"p-category\">Strategy</span>
  <span class=\"p-category\">Leadership</span>
</div>";

        let content = String::from(content_s);
        let root = super::mapparser::parse_string(content);
        let expected = json!({
            "items": [{ 
                "type": ["h-card"],
                "properties": {
                    "photo": ["https://webfwd.org/content/about-experts/300.mitchellbaker/mentor_mbaker.jpg"],
                    "name": ["Mitchell Baker"],
                    "url": [
                        "http://blog.lizardwrangler.com/",
                        "https://twitter.com/MitchellBaker"
                    ],
                    "org": ["Mozilla Foundation"],
                    "note": ["Mitchell is responsible for setting the direction and scope of the Mozilla Foundation and its activities."],
                    "category": [
                        "Strategy",
                        "Leadership"
                    ]
                }
            }]
        });

        println!("{}", serde_json::to_string_pretty(&root).unwrap());
        assert_json_eq!(root, expected);
    }

    #[test]
    fn h_event_location_h_card() {
        let content_s = "<div class=\"h-event\">
  <a class=\"p-name u-url\" href=\"http://indiewebcamp.com/2012\">
    IndieWebCamp 2012
  </a>
  from <time class=\"dt-start\">2012-06-30</time> 
  to <time class=\"dt-end\">2012-07-01</time> at 
  <span class=\"p-location h-card\">
    <a class=\"p-name p-org u-url\" href=\"http://geoloqi.com/\">
      Geoloqi
    </a>, 
    <span class=\"p-street-address\">920 SW 3rd Ave. Suite 400</span>, 
    <span class=\"p-locality\">Portland</span>, 
    <abbr class=\"p-region\" title=\"Oregon\">OR</abbr>
  </span>
</div>";

        let content = String::from(content_s);
        let root = super::mapparser::parse_string(content);
        let expected = json!({
            "items": [{ 
                "type": ["h-event"],
                "properties": {
                    "name": ["IndieWebCamp 2012"],
                    "url": ["http://indiewebcamp.com/2012"],
                    "start": ["2012-06-30"],
                    "end": ["2012-07-01"],
                    "location": [{
                        "value": "Geoloqi",
                        "type": ["h-card"],
                        "properties": {
                            "name": ["Geoloqi"],
                            "org": ["Geoloqi"],
                            "url": ["http://geoloqi.com/"],
                            "street-address": ["920 SW 3rd Ave. Suite 400"],
                            "locality": ["Portland"],
                            "region": ["Oregon"]
                        }
                    }]
                }
            }]
        });

        println!("{}", serde_json::to_string_pretty(&root).unwrap());
        assert_json_eq!(root, expected);
    }

    #[test]
    fn h_card_org_h_card() {
        let content_s = "<div class=\"h-card\">
  <a class=\"p-name u-url\"
     href=\"http://blog.lizardwrangler.com/\" 
    >Mitchell Baker</a> 
  (<span class=\"p-org\">Mozilla Foundation</span>)
</div>";

        let content = String::from(content_s);
        let root = super::mapparser::parse_string(content);
        let expected = json!({
            "items": [{ 
                "type": ["h-card"],
                "properties": {
                    "name": ["Mitchell Baker"],
                    "url": ["http://blog.lizardwrangler.com/"],
                    "org": ["Mozilla Foundation"]
                }
            }]
        });

        println!("{}", serde_json::to_string_pretty(&root).unwrap());
        assert_json_eq!(root, expected);
    }

    #[test]
    fn h_event_location_h_card_2() {
        let content_s = "<div class=\"h-card\">
  <a class=\"p-name u-url\"
     href=\"http://blog.lizardwrangler.com/\" 
    >Mitchell Baker</a> 
  (<a class=\"p-org h-card\" 
      href=\"http://mozilla.org/\"
     >Mozilla Foundation</a>)
</div>
";

        let content = String::from(content_s);
        let root = super::mapparser::parse_string(content);
        let expected = json!({
            "items": [{ 
                "type": ["h-card"],
                "properties": {
                    "name": ["Mitchell Baker"],
                    "url": ["http://blog.lizardwrangler.com/"],
                    "org": [{
                        "value": "Mozilla Foundation",
                        "type": ["h-card"],
                        "properties": {
                            "name": ["Mozilla Foundation"],
                            "url": ["http://mozilla.org/"]
                        }
                    }]
                }
            }]
        });

        println!("{}", serde_json::to_string_pretty(&root).unwrap());
        assert_json_eq!(root, expected);
    }

    #[test]
    fn h_entry_example() {
        let content_s = "<article class=\"h-entry\">
  <h1 class=\"p-name\">Microformats are amazing</h1>
  <p>Published by <a class=\"p-author h-card\" href=\"http://example.com\">W. Developer</a>
     on <time class=\"dt-published\" datetime=\"2013-06-13 12:00:00\">13<sup>th</sup> June 2013</time></p>
  
  <p class=\"p-summary\">In which I extoll the virtues of using microformats.</p>
  
  <div class=\"e-content\">
    <p>Blah blah blah</p>
  </div>
</article>";

        let content = String::from(content_s);
        let root = super::mapparser::parse_string(content);
        let expected = json!({
            "items": [
                {
                    "type": [
                        "h-entry"
                    ],
                    "properties": {
                        "name": [
                            "Microformats are amazing"
                        ],
                        "author": [
                            {
                                "value": "W. Developer",
                                "type": [
                                    "h-card"
                                ],
                                "properties": {
                                    "name": [
                                        "W. Developer"
                                    ],
                                    "url": [
                                        "http://example.com"
                                    ]
                                }
                            }
                        ],
                        "published": [
                            "2013-06-13 12:00:00"
                        ],
                        "summary": [
                            "In which I extoll the virtues of using microformats."
                        ],
                        "content": [
                            {
                                "value": "Blah blah blah",
                                "html": "<p>Blah blah blah</p>"
                            }
                        ]
                    }
                }
            ]
        });

        println!("{}", serde_json::to_string_pretty(&root).unwrap());
        assert_json_eq!(root, expected);
    }

}
