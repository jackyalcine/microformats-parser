use html5ever::rcdom::{Handle, NodeData};
use html5ever::tendril::{fmt::UTF8, Tendril};
use html5ever::Attribute;

use crate::microformats;
use microformats::Microformat;

#[derive(Debug)]
pub enum ParserContext<'a> {
    Root {
        item: &'a mut microformats::Root
    },
    Format {
        props: Vec<ParserProperty>,
        item: &'a mut Microformat
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum ParserProperty {
    EContent,
    DtPublished,
    DtUpdated,
    PCategory,
    PComment,
    PContent,
    PName,
    PSummary,
    UInReplyTo,
    ULikeOf,
    URepostOf,
    USyndication,
    UUrl
}

pub struct Parser<'a> {
    parent: Option<&'a NodeData>,
    handle: Handle,
}

impl<'a> Parser<'a> {
    pub fn start(handle: Handle) -> Self {
        Parser {
            handle,
            parent: None
        }
    }

    pub fn traverse(self, context: &mut ParserContext) {
        match self.handle.data {
            NodeData::Document => {
                //println!("(forward {:?}",context);
                self.call_children(context)
            },
            NodeData::Doctype { .. } => {
                //println!("(forward {:?}",context);
                self.call_children(context)
            },
            NodeData::Comment { .. } => {
                //println!("(forward {:?}",context);
                self.call_children(context)
            },

            NodeData::Text { ref contents } => {
                if let Some(NodeData::Element { ref name, .. }) = self.parent {
                    let tag_name = name.local.as_ref();

                    process_text(
                        context,
                        tag_name,
                        tendril_to_utf8(&contents.borrow()),
                    )
                }
                self.call_children(context)
            }

            NodeData::Element {
                ref name,
                ref attrs,
                ..
            } => {
                let tag_name = name.local.as_ref();
                let classes = get_classes(&attrs.borrow());
                let properties = properties_from_classes(&classes);

                if classes.contains(&String::from("h-entry")) {
                    let mut hentry = Microformat::new("h-entry");
                    let new_context = &mut ParserContext::Format{
                        props: properties,
                        item: &mut hentry
                    };

                    process_element(new_context,tag_name, &attrs.borrow());
                    self.call_children(new_context);

                    match context {
                        ParserContext::Root{item} => {
                            let e = match new_context {
                                ParserContext::Format{item, ..} => item,
                                _ => unreachable!()
                            };
                            item.items.push(e.clone());
                        },
                        _ => unreachable!()
                    }
                } else
                    if classes.contains(&String::from("h-cite")) {
                        let mut hcite = Microformat::new("h-cite");
                        let new_context = &mut ParserContext::Format{
                            props: properties,
                            item: &mut hcite
                        };

                        process_element(new_context,tag_name, &attrs.borrow());
                        self.call_children(new_context);

                        match context {
                            ParserContext::Format{item, ..} => {
                                let (e,props) = match new_context {
                                    ParserContext::Format{item, props} => (item, props),
                                    _ => unreachable!()
                                };
                                match item {
                                    Microformat::HEntry{properties,..} => {
                                        for p in props {
                                            match p {
                                                ParserProperty::UInReplyTo => properties.in_reply_to.push(e.clone()),
                                                ParserProperty::ULikeOf => properties.like_of.push(e.clone()),
                                                ParserProperty::URepostOf => properties.repost_of.push(e.clone()),
                                                ParserProperty::PComment => properties.comment.push(e.clone()),
                                                _=> println!("WARNING in cite : {:?}", e),
                                            }
                                        }
                                    },
                                    _ => ()
                                }
                            },
                            _ => unreachable!()
                        }
                    }
                else if classes.contains(&String::from("h-card")) {
                    let mut hcard = Microformat::new("h-card");
                    let new_context = &mut ParserContext::Format{
                        props: properties,
                        item: &mut hcard
                    };

                    process_element(new_context,tag_name, &attrs.borrow());
                    self.call_children(new_context);

                    match context {
                        ParserContext::Root{item} => {
                            let e = match new_context {
                                ParserContext::Format{item, ..} => item,
                                _ => unreachable!()
                            };
                            item.items.push(e.clone());
                        },
                        ParserContext::Format{item, ..} => {
                            let e = match new_context {
                                ParserContext::Format{item, ..} => item,
                                _ => unreachable!()
                            };
                            match item {
                                Microformat::HEntry{properties,..} => properties.author = e.clone(),
                                Microformat::HCite{properties,..} => {
                                    let p = properties;
                                    if let Microformat::HCard{properties, ..} = &p.author {
                                        if properties.name.is_empty() && properties.url.is_empty() {
                                            p.author = e.clone()
                                        }
                                    }
                                },
                                _ => println!("WARNING in card : {:?}", e)
                            }
                        },
                    }
                }

                // properties

                else {
                    let propagated_props = process_element(context,tag_name, &attrs.borrow());

                    if !propagated_props.is_empty()  {
                        if let ParserContext::Format{item, ..} = context {

                            let mut new_context = ParserContext::Format{
                                props: propagated_props,
                                item: item
                            };
                            self.call_children(&mut new_context);

                        } else {
                            self.call_children(context);
                        }
                    } else {
                        self.call_children(context);
                    }
                }
            },
            NodeData::ProcessingInstruction { .. } => unreachable!(),
        }
    }
    fn call_children(&self, context: &mut ParserContext) {
        for child in self.handle.children.borrow().iter() {
            let new_parser = Parser {
                parent: Some(&self.handle.data),
                handle: child.clone(),
            };

            new_parser.traverse(context);

        }
    }
}

fn process_text(context: &mut ParserContext, _tag_name: &str, contents: &str) {
    // println!("{:?}", context);
    match context {
        ParserContext::Format {item, props} => {
            for prop in props {
                set_value(item, &prop, contents.to_string(), false);
            }
        },
        _ => ()
    }
}

fn set_value(item: &mut Microformat, prop : &ParserProperty, value : String, insertion : bool) {
    match prop {
        ParserProperty::EContent => {
            match item {
                Microformat::HEntry{properties,..} => properties.content.value.push_str(&value.trim()),
                Microformat::HCite{properties,..} => insert_or_extend(&mut properties.content, value, insertion),
                _ => (),
            }
        },
        ParserProperty::DtPublished => {
            match item {
                Microformat::HEntry{properties,..} => insert_or_extend(&mut properties.published, value, insertion),
                Microformat::HCite{properties,..} => insert_or_extend(&mut properties.published,value, insertion),
                _ => (),
            }
        },
        ParserProperty::DtUpdated => {
            match item {
                Microformat::HEntry{properties,..} => insert_or_extend(&mut properties.updated, value, insertion),
                _ => (),
            }
        },
        ParserProperty::PCategory => {
            match item {
                Microformat::HEntry{properties,..} => insert_or_extend(&mut properties.category, value, insertion),
                _ => (),
            }
        },
        ParserProperty::PContent => {
            match item {
                Microformat::HCite{properties,..} => insert_or_extend(&mut properties.content, value, insertion),
                _ => (),
            }
        },
        ParserProperty::PName => {
            match item {
                Microformat::HEntry{properties,..} => insert_or_extend(&mut properties.name, value, insertion),
                Microformat::HCard{properties,..} => insert_or_extend(&mut properties.name, value, insertion),
                Microformat::HCite{properties,..} => insert_or_extend(&mut properties.name, value, insertion),
            }
        },
        ParserProperty::PSummary => {
            match item {
                Microformat::HEntry{properties,..} => insert_or_extend(&mut properties.summary, value, insertion),
                _ => (),
            }
        },
        ParserProperty::USyndication => {
            match item {
                Microformat::HEntry{properties,..} => insert_or_extend(&mut properties.syndication, value, insertion),
                _ => (),
            }
        },
        ParserProperty::UUrl => {
            match item {
                Microformat::HCard{properties,..} => insert_or_extend(&mut properties.url, value, insertion),
                Microformat::HCite{properties,..} => insert_or_extend(&mut properties.url, value, insertion),
                Microformat::HEntry{properties,..} => insert_or_extend(&mut properties.url, value, insertion),
            }
        },
        _ => ()

    }
}

fn insert_or_extend(prop_values :&mut Vec<String>, value: String, insertion: bool) {
    if insertion {
        prop_values.push(value);
    } else {
        if let Some(last_value) = prop_values.last_mut() {
            last_value.push_str(&value);
        } else {
            println!("unintiated property values {:?} on {} extention", prop_values, value);
        }
    }
}

fn properties_from_classes(classes: &Vec<String>) -> Vec<ParserProperty> {
    let mut results = Vec::new();
    for class in classes {
        match class.as_str() {
            "dt-published" => results.push(ParserProperty::DtPublished),
            "dt-updated" => results.push(ParserProperty::DtUpdated),
            "e-content" => results.push(ParserProperty::EContent),
            "p-category" => results.push(ParserProperty::PCategory),
            "p-comment" => results.push(ParserProperty::PComment),
            "p-content" => results.push(ParserProperty::PContent),
            "p-name" => results.push(ParserProperty::PName),
            "p-summary" => results.push(ParserProperty::PSummary),
            "p-in-reply-to" => results.push(ParserProperty::UInReplyTo),
            "u-in-reply-to" => results.push(ParserProperty::UInReplyTo),
            "p-like-of" => results.push(ParserProperty::ULikeOf),
            "u-like-of" => results.push(ParserProperty::ULikeOf),
            "p-repost-of" => results.push(ParserProperty::URepostOf),
            "u-repost-of" => results.push(ParserProperty::URepostOf),
            "u-syndication" => results.push(ParserProperty::USyndication),
            "u-url" => results.push(ParserProperty::UUrl),
            _ => ()
        }
    }
    return results;
}

fn process_element(
    context: &mut ParserContext,
    tag_name: &str,
    attrs: &[Attribute],
) -> Vec<ParserProperty> {
    let properties = properties_from_classes(&get_classes(attrs));
    let mut propagated_properties = Vec::new();

    // initialize all the property values
    if let ParserContext::Format{item, ..} = context {
        for prop in properties {
            let is_done = match &prop {
                ParserProperty::DtPublished | ParserProperty::DtUpdated => {
                    match tag_name {
                        "time" | "ins" | "del" => set_attrs_value(item, &prop, attrs, "datetime"),
                        "abbr" => set_attrs_value(item, &prop, attrs, "title"),
                        "data" | "input" => set_attrs_value(item, &prop, attrs, "value"),
                        _ => {
                            set_value(item, &prop, String::new(), true);
                            false
                        }
                    }
                },
                ParserProperty::PCategory | ParserProperty::PContent | ParserProperty::PName | ParserProperty::PSummary  => {
                    match tag_name {
                        "img" | "area" => set_attrs_value(item, &prop, attrs, "alt"),
                        "data" | "input" => set_attrs_value(item, &prop, attrs, "value"),
                        "abbr" | "link" => set_attrs_value(item, &prop, attrs, "title"),
                        _ => {
                            set_value(item, &prop, String::new(), true);
                            false
                        }
                    }
                },
                ParserProperty::UUrl | ParserProperty::USyndication => {
                    match tag_name {
                        "a" | "area" | "link" => set_attrs_value(item, &prop, attrs, "href"),
                        "img" => set_attrs_value(item, &prop, attrs, "src"),
                        "audio" | "video" | "source" | "iframe" => set_attrs_value(item, &prop, attrs, "src"),
                        "object" => set_attrs_value(item, &prop, attrs, "data"),
                        "abbr" => set_attrs_value(item, &prop, attrs, "title"),
                        "data" | "input" => set_attrs_value(item, &prop, attrs, "value"),
                        _ => {
                            set_value(item, &prop, String::new(), true);
                            false
                        }
                    }
                },
                _ => {
                    set_value(item, &prop, String::new(), true);
                    false
                }
             };
            if ! is_done {
                propagated_properties.push(prop.clone());
            }
        }
    }
    return propagated_properties;
}

fn set_attrs_value(item: &mut Microformat, prop : &ParserProperty, attrs: &[Attribute], name : &str) -> bool {
    if let Some(value) = get_attribute(attrs, name) {
        set_value(item, prop, value, true);
        return true;
    } else {
        set_value(item, prop, String::new(), true);
        return false;
    }
}

fn get_attribute(attrs: &[Attribute], name: &str) -> Option<String> {
    attrs
        .iter()
        .filter(|attr| attr.name.local.as_ref() == name)
        .nth(0)
        .and_then(|attr| Some(attr.value.trim().to_string()))
}

fn get_classes(attrs: &[Attribute]) -> Vec<String> {
    get_attribute(attrs, "class")
        .unwrap_or(String::new())
        .split_whitespace().map(str::to_string).collect()
}

// fn text_content(handle: &Handle) -> Option<String> {
//     // todo paste all the text together
//     for child in handle.children.borrow().iter() {
//         if let NodeData::Text { ref contents } = child.data {
//             let string = tendril_to_utf8(&contents.borrow()).to_string();
//             return Some(string.trim().to_string());
//         }
//     }

//     None
// }

fn tendril_to_utf8(t: &Tendril<UTF8>) -> &str {
    t
}
