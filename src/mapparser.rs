use html5ever::rcdom::{Handle, NodeData};
use html5ever::tendril::{fmt::UTF8, Tendril};
use html5ever::Attribute;
use html5ever::parse_document;
use html5ever::driver::ParseOpts;
use html5ever::rcdom::RcDom;
use html5ever::tendril::TendrilSink;

use std::collections::HashMap;

use serde_json::json;
use serde_json::Value;


pub fn parse_string(content : String) -> Value {

    let dom = parse_document(RcDom::default(), ParseOpts::default())
        .from_utf8()
        .read_from(&mut content.as_bytes());
    
    let parser = Parser::start(dom.unwrap().document);

    let mut item = json!{{
        "items": []
    }};
    let mut context = ParserContext {
        implied_props: HashMap::new(),
        in_h: false,
        is_p_init: false,
        is_u_init: false,
        props: Vec::new(),
        item: &mut item.clone(),
        root: &mut item
    };
    parser.traverse(&mut context);

    context.root.clone()
}

#[derive(Debug)]
pub struct ParserContext<'a> {
    pub in_h: bool,
    pub is_p_init: bool,
    pub is_u_init: bool,
    pub implied_props: HashMap<String, String>,
    pub props: Vec<String>,
    pub item: &'a mut Value,
    pub root: &'a mut Value,
}

pub struct Parser<'a> {
    parent: Option<&'a NodeData>,
    handle: Handle,
}

impl<'a> Parser<'a> {
    pub fn start(handle: Handle) -> Self {
        Parser {
            handle,
            parent: None
        }
    }

    pub fn traverse(self, context: &mut ParserContext) {
        match self.handle.data {
            NodeData::Document => {
                self.call_children(context)
            },
            NodeData::Doctype { .. } => {
                self.call_children(context)
            },
            NodeData::Comment { .. } => {
                self.call_children(context)
            },

            NodeData::Text { ref contents } => {
                if context.in_h {
                    if let Some(NodeData::Element { ref name, .. }) = self.parent {
                        let tag_name = name.local.as_ref();

                        process_text(
                            context,
                            tag_name,
                            tendril_to_utf8(&contents.borrow()),
                        )
                    }
                }
                self.call_children(context)
            }

            NodeData::Element {
                ref name,
                ref attrs,
                ..
            } => {
                let tag_name = name.local.as_ref();
                let classes = get_classes(&attrs.borrow());
                let h_classes = filter_by_pref(&classes, "h-");
                let properties = properties_from_classes(classes);

                if !h_classes.is_empty() {
                    let mut mf = create_mf(&h_classes);
                    let new_context = &mut ParserContext{
                        is_p_init: false,
                        is_u_init: false,
                        in_h: true,
                        implied_props: HashMap::new(),
                        props: properties,
                        item: &mut mf,
                        root: context.root
                    };

                    process_element(new_context, tag_name, &attrs.borrow(), true);
                    self.call_children(new_context);
                    post_process_h_element(new_context);
                    
                    let mut new_item = new_context.item.clone();
                    if let Value::Object(ref mut obj) = context.item {
                        if let Some(p) = new_context.props.first() {
                            if let Some(properties) = obj.get_mut("properties") {
                                if let Value::Object(props) = properties {

                                    let key = prop_to_key(p);
                                    // handle value property by catching the corresponding
                                    // property value in the child
                                    if let Value::Object(ref mut new_obj) = new_item {
                                        if let Some(Value::Object(child_properties)) = new_obj.get_mut("properties") {
                                            child_properties.remove(&key);
                                        }
                                        if let Some(Value::Object(child_properties)) = new_obj.get("properties") {
                                            if p.starts_with("p-") {
                                                if let Some(value) = child_properties.get("name") {
                                                    new_obj.insert(String::from("value"), value.get(0).unwrap().clone());
                                                }
                                            }
                                        }
                                    }

                                    let child = Value::from(vec![new_item.clone()]);
                                    props.insert(key, child);
                                }
                            } else if let Some(items) = context.root.get_mut("items") {
                                if let Value::Array(ref mut vec) = items {
                                    vec.push(new_item);
                                }
                            }
                        } else if let Some(items) = context.root.get_mut("items") {
                            if let Value::Array(ref mut vec) = items {
                                vec.push(new_item);
                            }
                        }
                    }
                }
                // properties
                else {
                    let propagated_props = process_element(context,tag_name, &attrs.borrow(), false);

                    if !propagated_props.is_empty() && context.in_h {

                        let mut e_props = Vec::new();
                        for prop in &propagated_props {
                            if prop.starts_with("e-") {
                                e_props.push(prop);
                            }
                        }

                        if !e_props.is_empty() {
                            let mut e_context = EParserContext{
                                    html: String::new(),
                                    value: String::new(),
                                };
                                let e_parser = EParser::start(self.handle);
                                e_parser.traverse(&mut e_context);

                            let elt = json!(
                                [
                                    {
                                        "html": e_context.html,
                                        "value": e_context.value
                                    }
                                ]
                            );

                            for e_prop in e_props {
                                context.item.as_object_mut().unwrap().get_mut("properties").unwrap().as_object_mut().unwrap().insert(prop_to_key(e_prop), elt.clone());
                            }

                        } else {
                            let mut new_context = ParserContext{
                                in_h: true,
                                is_p_init: false,
                                is_u_init: false,
                                implied_props: HashMap::new(),
                                props: propagated_props,
                                item: context.item,
                                root: context.root
                            };

                            self.call_children(&mut new_context);
                        }
                    } else {
                        self.call_children(context);
                    }
                }
            },
            NodeData::ProcessingInstruction { .. } => unreachable!(),
        }
    }
    fn call_children(&self, context: &mut ParserContext) {
        for child in self.handle.children.borrow().iter() {
            let new_parser = Parser {
                parent: Some(&self.handle.data),
                handle: child.clone(),
            };
            new_parser.traverse(context);
        }
    }
}

// EParser

#[derive(Debug)]
pub struct EParserContext {
    html: String,
    value: String
}
pub struct EParser<'a> {
    parent: Option<&'a NodeData>,
    handle: Handle,
}

impl<'a> EParser<'a> {
    pub fn start(handle: Handle) -> Self {
        EParser {
            handle,
            parent: None
        }
    }

    pub fn traverse(self, context: &mut EParserContext) {
        match self.handle.data {
            NodeData::Text { ref contents } => {
                context.html.push_str(tendril_to_utf8(&contents.borrow()).trim());
                context.value.push_str(tendril_to_utf8(&contents.borrow()).trim());
                self.call_children(context)
            },

            NodeData::Element {
                ref name,
                ref attrs,
                ..
            } => {
                self.call_children(context);
                let mut attributes = String::new();
                if self.parent.is_some() {
                    for attr in attrs.borrow().iter() {
                        attributes.push_str(&format!(" {}=\"{}\"", attr.name.local, attr.value));
                    }
                    context.html = format!("<{}{}>{}</{}>", name.local, attributes, context.html, name.local);
                }
            },
            _ => ()
        }
    }
    fn call_children(&self, context: &mut EParserContext) {
        for child in self.handle.children.borrow().iter() {
            let new_parser = EParser {
                parent: Some(&self.handle.data),
                handle: child.clone(),
            };
            new_parser.traverse(context);
        }
    }
}


fn process_text(context: &mut ParserContext, _tag_name: &str, contents: &str) {
    
    for prop in &context.props {
        set_value(&mut context.item, prop, contents.trim().to_string(), false);
    }

    // implied properties
    if !context.implied_props.contains_key("name") {
        context.implied_props.insert(String::from("name"), String::from(contents));
    }
}

fn set_value(item: &mut Value, prop : &String, value : String, insertion : bool) {
    if let Value::Object(map) = item {
        if let Some(properties) = map.get_mut("properties") {
            insert_or_extend(properties, prop, value, insertion);
        }
    }
}

fn insert_or_extend(properties:&mut Value, p: &String, value: String, insertion: bool) {

    let key = prop_to_key(p);

    if let Value::Object(obj) = properties {
        if let Some(Value::Array(prop_values)) = obj.get_mut(&key) {
            if !insertion {
                if let Some(Value::String(last_value)) = prop_values.last_mut() {
                    last_value.push_str(&value);
                } else {
                    println!("unintiated property values {:?} on {} extention", prop_values, value);
                }
            } else {
                prop_values.push(Value::from(value));
            }
        } else {
            let mut vec = Vec::new();
            vec.push(Value::from(value));
            let prop_values = Value::from(vec);
            obj.insert(key, prop_values);
        }
    }
}

fn properties_from_classes(classes: Vec<String>) -> Vec<String> {
    let mut results = Vec::new();
    for class in classes {
        if class.starts_with("p-") || class.starts_with("e-") || class.starts_with("u-") || class.starts_with("dt-") {
            results.push(class);
        }
    }
    return results;
}

fn process_element(
    context: &mut ParserContext,
    tag_name: &str,
    attrs: &[Attribute],
    is_new_h: bool
) -> Vec<String> {
    let properties = properties_from_classes(get_classes(attrs));
    let mut propagated_properties = Vec::new();

    // initialize all the property values
    for prop in properties {
        let is_dt_set = if prop.starts_with("dt-") {
            match tag_name {
                "time" | "ins" | "del" => set_attrs_value(context.item, &prop, attrs, "datetime"),
                "abbr" => set_attrs_value(context.item, &prop, attrs, "title"),
                "data" | "input" => set_attrs_value(context.item, &prop, attrs, "value"),
                _ => {
                    set_value(&mut context.item, &prop, String::new(), true);
                    false
                }
            }
        } else {
            false
        }; 

        let is_p_init = if prop.starts_with("p-") {
            match tag_name {
                "img" | "area" => set_attrs_value(context.item, &prop, attrs, "alt"),
                "data" | "input" => set_attrs_value(context.item, &prop, attrs, "value"),
                "abbr" | "link" => set_attrs_value(context.item, &prop, attrs, "title"),
                _ => {
                    set_value(&mut context.item, &prop, String::new(), true);
                    false
                }
            }
        } else {
            false
        };

        let is_u_init = if prop.starts_with("u-") {
            match tag_name {
                "a" | "area" | "link" => set_attrs_value(context.item, &prop, attrs, "href"),
                "img" => set_attrs_value(context.item, &prop, attrs, "src"),
                "audio" | "video" | "source" | "iframe" => set_attrs_value(context.item, &prop, attrs, "src"),
                "object" => set_attrs_value(context.item, &prop, attrs, "data"),
                "abbr" => set_attrs_value(context.item, &prop, attrs, "title"),
                "data" | "input" => set_attrs_value(context.item, &prop, attrs, "value"),
                _ => {
                    set_value(&mut context.item, &prop, String::new(), true);
                    false
                }
            }
        } else {
            false
        };

        if !is_dt_set && !is_p_init && !is_u_init {
            propagated_properties.push(prop.clone());
        }

        // if it is a h-* element the properties are used for the value property.
        context.is_p_init = !is_new_h && (context.is_p_init || prop.starts_with("p-"));
        context.is_u_init = !is_new_h && (context.is_u_init || prop.starts_with("u-"));
    }

    // handle implied property
    // name property
    if !context.is_p_init {

        match tag_name {
            "img" | "area" => if let Some(value) = get_attribute(attrs, "alt") {
                context.implied_props.insert(String::from("name"), value);
                // context.is_p_init = true;
            },
            "abbr" => if let Some(value) = get_attribute(attrs, "title") {
                context.implied_props.insert(String::from("name"), value);
                // context.is_p_init = true;
            },
            _ => ()
        }
    }
    // u-* implied properties
    if !context.is_u_init {
        match tag_name {
            "a" | "area" => {
                if let Some(value) = get_attribute(attrs, "href") {
                    context.implied_props.insert(String::from("url"), value);
                }
            },
            "img" => {
                if let Some(value) = get_attribute(attrs, "src") {
                    context.implied_props.insert(String::from("photo"), value);
                }
            },
            "object" => {
                if let Some(value) = get_attribute(attrs, "data") {
                    context.implied_props.insert(String::from("photo"), value);
                }
            },
            _ => ()
        }
    }
    return propagated_properties;
}

fn post_process_h_element(context: &mut ParserContext) {
    if let Value::Object(obj) = context.item {
        if let Value::Object(properties) = obj.get_mut("properties").unwrap() {

            if !context.is_p_init {
                if let Some(name) = context.implied_props.get("name") {
                    let value = Value::from(vec![Value::from(name.to_string())]);
                    properties.insert(String::from("name"), value);
                }
            }

            if !context.is_u_init {
                if let Some(url) = context.implied_props.get("url") {
                    let value = Value::from(vec![Value::from(url.to_string())]);
                    properties.insert(String::from("url"), value);
                }

                if let Some(photo) = context.implied_props.get("photo") {
                    let value = Value::from(vec![Value::from(photo.to_string())]);
                    properties.insert(String::from("photo"), value);
                }
            }
        }
    }
}


fn set_attrs_value(item: &mut Value, prop : &String, attrs: &[Attribute], name : &str) -> bool {
    if let Some(value) = get_attribute(attrs, name) {
        set_value(item, prop, value, true);
        return true;
    } else {
        set_value(item, prop, String::new(), true);
        return false;
    }
}

fn get_attribute(attrs: &[Attribute], name: &str) -> Option<String> {
    attrs
        .iter()
        .filter(|attr| attr.name.local.as_ref() == name)
        .nth(0)
        .and_then(|attr| Some(attr.value.trim().to_string()))
}

fn get_classes(attrs: &[Attribute]) -> Vec<String> {
    get_attribute(attrs, "class")
        .unwrap_or(String::new())
        .split_whitespace().map(str::to_string).collect()
}

fn prop_to_key(prop: &String) -> String {
    let prefix: Vec<&str> = prop.split("-").collect();
    String::from(prop.strip_prefix(prefix[0]).unwrap().strip_prefix("-").unwrap())
}

fn filter_by_pref(values: &Vec<String>, pref:&str) -> Vec<String> {
    let mut result = Vec::new();
    for v in values {
        if v.starts_with(pref) {
            result.push(v.clone())
        }
    }
    result
}

fn create_mf(h_classes: &Vec<String>) -> Value {
    json!({
        "type": h_classes,
        "properties": {}
    })
}

fn tendril_to_utf8(t: &Tendril<UTF8>) -> &str {
    t
}
